import { api } from './api'

export default {
    getBrands: () => {
        return api.get('/fipe/api/v1/carros/marcas')
    },

    getModelsForId: (id) => {
        return api.get(`/fipe/api/v1/carros/marcas/${id}/modelos`)
    } 
}