## Car_brands_project 
 
Projeto simples consumindo API e retornando tabela de dados na tela. 
 
 
## Tecnologias
 
Aqui as tecnologias usadas no projeto.
 
* Vue  2.6.11
* Vuetifyjs 2.2.11

 
## Repositório usado
 
* Gitlab
 
## Como Iniciar
 

* Após baixar a aplicação:
>    $ cd car-brands-project

* Necessário axios:
>    $ npm install axios

* Para instalar:
>    $ npm install

* Para rodar o projeto:
>    $ npm run serve
 
## Como visualizar
 
Você pode visualizar o projeto em seu browser após ele ser totalmente compilado no caminho 'localhost:8080'